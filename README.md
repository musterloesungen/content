# content



Dieses Projekt enthält zwei einfache HTML-Dateien, die jeweils minimalen JavaScript-Code einbinden. Die HTML-Dateien dienen als Basis für die Bereitstellung einer Webanwendung innerhalb eines Kubernetes-Clusters. Ziel ist es, die beiden Websites über Kubernetes zugänglich zu machen.
